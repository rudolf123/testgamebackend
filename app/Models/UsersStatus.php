<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersStatus extends Model
{
    protected $table = 'users_status';
    protected  $fillable = ['user_id', 'position_x', 'energy'];
    use HasFactory;
}
