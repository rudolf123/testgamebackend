<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoomStatus extends Model
{
    protected $table = 'user_room_statuses';
    protected  $fillable = ['user_id', 'room_id', 'offset'];
    use HasFactory;
}
