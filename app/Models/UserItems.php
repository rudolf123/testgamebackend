<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserItems extends Model
{
    protected $table = 'user_items';
    protected  $fillable = ['user_id', 'item_id'];
    use HasFactory;
}
