<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\RoomStatus;

class RoomStatusController extends Controller
{
    public function index()
    {
        $room_statuses = RoomStatus::all();
        return $room_statuses;
    }

    public function show($id)
    {
        $room_status = RoomStatus::where('room_id', $id)->first();
        if (!$room_status) {
            return response()->json(null);
        }
        return response()->json($room_status);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $room_status = RoomStatus::create($data);
        return response(['message' => 'Created successfully'], 201);
    }

    public function update(Request $request)
    {
        $room_status = RoomStatus::where('room_id', $request->room_id)->first();
        $data = $request->all();
        $room_status->fill($data);
        $room_status->save();
        return $room_status;
    }

    public function updateStatus(Request $request)
    {
        $room_status = RoomStatus::where('room_id', $request->room_id)->first();
        $data = $request->all();
        $room_status->fill($data);
        $room_status->save();
        return $room_status;
    }
}
