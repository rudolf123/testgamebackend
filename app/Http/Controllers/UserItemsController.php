<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\UserItems;

class UserItemsController extends Controller
{
    public function index()
    {
        $users_items = UserItems::all();
        return response()->json($users_items);
    }

    public function show($id)
    {
        $user_items = UserItems::where('user_id', $id)->get();
        if (!$user_items) {
            return response()->json(null);
        }
        return response()->json($user_items);
    }

    public function store(Request $request)
    {
        $item = UserItems::where('item_id', $request->item_id)->where('user_id', $request->user_id)->first();
        if (!$item) {
            $data = $request->all();
            $new_item = UserItems::create($data);
            $new_item->count = 1;
            $new_item->save();
            response()->json($new_item);
        }
        else{
            $data = $request->all();
            $item->count++;
            $item->save();
            response()->json($item);
        }
    }

    public function update($id)
    {
        $users_status = UsersStatus::find($id);
        $users_status->user_id = Input::get('user_id');
        $users_status->room_id = Input::get('room_id');
        $users_status->offset = Input::get('offset');
        $users_status->save();
        return $users_status;
    }

    public function updateStatus(Request $request)
    {
        $users_status = UsersStatus::where('user_id', $request->user_id)->first();
        $data = $request->all();
        $users_status->fill($data);
        $users_status->save();
        return $users_status;
    }
}
