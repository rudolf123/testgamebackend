<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\UsersStatus;

class UsersStatusController extends Controller
{
    public function index()
    {
        $users_statuses = UsersStatus::all();
        return $users_statuses;
    }

    public function show($id)
    {
        $users_status = UsersStatus::where('user_id', $id)->first();
        if (!$users_status) {
            return response()->json(null);
        }
        return response()->json($users_status);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $users_status = UsersStatus::create($data);
        return response(['message' => 'Created successfully'], 201);
    }

    public function update(Request $request)
    {
        $users_status = UsersStatus::where('user_id', $request->user_id)->first();
        $data = $request->all();
        $users_status->fill($data);
        $users_status->save();
        return $users_status;
    }

    public function updateStatus(Request $request)
    {
        $users_status = UsersStatus::where('user_id', $request->user_id)->first();
        $data = $request->all();
        $users_status->fill($data);
        $users_status->save();
        return $users_status;
    }
}
